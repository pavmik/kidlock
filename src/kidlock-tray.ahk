﻿Menu, Tray, Icon, unlock.ico ; Change tray icon
Menu, Tray, Tip, Press Ctrl+Alt+L to lock your keyboard

^!l::
Menu, Tray, Icon, lock.ico ; Change tray icon
Menu, Tray, Tip, Computer is locked
BlockInput, On
Return

^!u::
Menu, Tray, Icon, unlock.ico ; Change tray icon
BlockInput, Off
Return

