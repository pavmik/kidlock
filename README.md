# Kidlock

## Block keyboard and mouse while your kid is watching something on a computer

### Common scenarios
- Have your kids watch a movie and block the keyboard
- Your kids can safely Skype their grandparents without accidentally close connectio

### Howto
- download https://gitlab.com/pavmik/kidlock/-/archive/master/kidlock-master.zip
- unzip
- run kidlock.exe (keyboard locking require admin rights)
- lock with pressing ctrl+l
- unlock with pressing ctrl+u
- lock state is indicating with systray icon


### Customization
- install AutoHotKey https://www.autohotkey.com/
- edit *.ahk files
- run ahk script with AutoHotKey or compile with "Convert .ahk to .exe".


### Licence
Program is compiled with AutoHotKey https://www.autohotkey.com/ licenced with GNU General Public License v2.0
#### Source code
- https://github.com/Lexikos/AutoHotkey_L
- https://gitlab.com/pavmik/kidlock


